public class CaseRelatedProductsController {
    public static Id caseID {get; private set;}
    public List<productWrapper> productList {get; private set;}

    public CaseRelatedProductsController(){
     	caseID = Apexpages.currentPage().getParameters().get('Id');
        productList = generateRelatedProductList();

    }

    @AuraEnabled
    static public List<productWrapper> getRelatedProductList(Id newCaseId) {
        caseID = newCaseId;
        return generateRelatedProductList();
    }

    private static List<productWrapper> generateRelatedProductList() {
        List<productWrapper> returnList = new List<productWrapper>();

        //query to get current case details
        Case compCase = [Select Id, AccountID from Case where Id = :caseId];
        String caseSensitiveID = ((String)compcase.AccountId).left(15);
        system.debug('accountID ' + compCase.AccountId );
        //retrieve all the opportunity line items related to the account on the case
        for(OpportunityLineItem oli : [Select Id, Product2.Name, Product2Id, Opportunity.CloseDate
                                       From OpportunityLineItem
                                       Where
                                 	       OpportunityLineItem.AccountId__c = :caseSensitiveID AND
                                       	   Opportunity.isWon = true
                                       Order By
                                       		Opportunity.CloseDate desc
                                      ])
        {
            system.debug('in wrapper for oli ' + oli.Id);
        	returnList.add(new productWrapper(oli));
        }

        return returnList;
    }

    public class productWrapper {
        @AuraEnabled public string productName {get; private set;}
        @AuraEnabled public string purchasedDate {get; private set;}
        @AuraEnabled public string productId {get; private set;}

        public productWrapper(OpportunityLineItem oli) {
            productName = oli.Product2.Name;
            purchasedDate = oli.Opportunity.CloseDate.format();
            productId = oli.Product2Id;
        }

    }
}